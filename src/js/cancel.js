var lastCardDrawn = "";

var backupDrawnCards = [];

if(localStorage.getItem("Cards") != ""){
    backupDrawnCards = [localStorage.getItem("Cards")];
}

function getLastCardDrawn(card){

    lastCardDrawn = card[0].code;

    var cardPicture = card[0].image;

    backupDrawnCards.push(cardPicture);

    localStorage.setItem("Cards", backupDrawnCards);
    
}


function cancel(){

    increment = increment - 1;

    if(localStorage.getItem("cardValues") != ""){
        cardValues = [localStorage.getItem("cardValues")];
    }

    if(cardValues.length >= 1){

        ReturnCardToDeck(deckId);
    
        calculateScoreAfterCancel();
        
    }
    

    localStorage.setItem('score', totalScore);

    if(backupDrawnCards != [""]){
        backupDrawnCards.pop();
        localStorage.setItem("Cards", backupDrawnCards);
    }
}


async function ReturnCardToDeck(deck_id) {
    
    return fetch(`https://deckofcardsapi.com/api/deck/${deck_id}/return/?cards=${lastCardDrawn}`, {
        method: "GET",
        mode: "cors",
        headers: {
            Accept: "application.json"
        }
    }).then(response => {
        if(response.ok) {
            return response.json();
        } 
            return Promise.reject(new Error("La carte n'a pas pu être remise dans le paquet"));
    }).then((datas) => {
            returnDisplayCardToDeck(datas)
            return datas;
    }).catch(function(error) {
        alert(error.message);
    });
}

function returnDisplayCardToDeck(datas){
    var numberAfterReturnCard = datas.remaining;
    const containerDrawCard = document.getElementById("container-draw-card");
    containerDrawCard.lastChild.remove();
    document.getElementById("rest-card").innerHTML =  numberAfterReturnCard + " cartes restante(s)";
}