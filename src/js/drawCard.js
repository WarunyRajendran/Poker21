var card;
var increment = 0;
var cardArbitrary = 0;

card = localStorage.getItem('Cards');


async function callDrawCard(){

    if(currentDevice.match(regex)){
        vibrate();
    }

    if ( cardArbitrary == 0 ) {
        increment = increment + 1;
        var draw_card = await drawCard(deckId);
        displayCard(draw_card);
        calculateScore(card);
        displayRestartCancelButton();
        
    }
    else {
        var draw_card = await drawCardNumberCardArbitrary(deckId, cardArbitrary);
        displayCardNumberCardArbitrary(draw_card, cardArbitrary);
        calculateScoreNumberCardArbitrary(card, cardArbitrary);
        displayRestartCancelButton();
    }
    
}

async function callDrawCardAfterStop(){

    if ( cardArbitrary == 0) {
        await drawCard(deckId);
        calculateScoreAfterStop(card);
    } else {
        await drawCardNumberCardArbitrary(deckId, cardArbitrary);
        calculateScoreNumberCardArbitraryeAfterStop(card, cardArbitrary);
    }
}

async function drawCard(deckId){
    return fetch(`https://deckofcardsapi.com/api/deck/${deckId}/draw/?count=1`,{
        method: "GET",
        mode: "cors",
        headers: {
            Accept: "application.json"
        }

    }).then(response => {
        if(response.ok) {
            return response.json();
        } 
            return Promise.reject(new Error("La carte n'a pas pu être tirée"));
    })
    .then((datas) => {
        card = datas.cards;
        setValues(card);
        getLastCardDrawn(card);
        var valueToSave = card[0].score;
        cardValues.push(valueToSave);
        localStorage.setItem('cardValues', cardValues);
        cardValues = [localStorage.getItem("cardValues")];
        
        return datas;
    }).catch(function(error) {
        alert(error.message); 
    });
}

async function drawCardNumberCardArbitrary(deckId, cardArbitrary){
    return fetch(`https://deckofcardsapi.com/api/deck/${deckId}/draw/?count=${cardArbitrary}`,{
        method: "GET",
        mode: "cors",
        headers: {
            Accept: "application.json"
        }

    }).then(response => {
        if(response.ok) {
            return response.json();
        } 
            return Promise.reject(new Error("La carte n'a pas pu être tirée"));
    })
    .then((datas) => {
        card = datas.cards;
        setValuesNumberCardArbitrary(card, cardArbitrary);
        getLastCardDrawn(card);
        
        return datas;
    }).catch(function(error) {
        alert(error.message);
    });
} 

function displayRestartCancelButton(){
    var btnRestart = document.getElementById("restart");
    var btnCancel = document.getElementById("cancel");
    var btnStop =  document.getElementById("btn-stop");
    var btnDraw = document.getElementById("draw");

    btnRestart.removeAttribute("style");
    btnCancel.removeAttribute("hidden");
    btnCancel.removeAttribute("style");
    btnStop.removeAttribute("style");
    btnDraw.removeAttribute("style");
}

function displayCard(draw_card){
    const containerDrawCard = document.getElementById("container-draw-card");
    let img = document.createElement("img");
    img.setAttribute("id", increment);
    let numberRestCard = draw_card.remaining;
    img.src = draw_card.cards[0].image;
    containerDrawCard.appendChild(img);
    document.getElementById("rest-card").innerHTML =  numberRestCard + " cartes restante(s)";
}

function displayCardNumberCardArbitrary(draw_card, cardArbitrary){
    const containerDrawCard = document.getElementById("container-draw-card");
    let numberRestCard = draw_card.remaining;
    for(let i = 0; i < cardArbitrary; i++) {
        let img = document.createElement("img");
        img.src = draw_card.cards[i].image;
        containerDrawCard.appendChild(img);
    }
    document.getElementById("rest-card").innerHTML =  numberRestCard + " cartes restante(s)";
} 