var deckId = 0;
var stopButton = document.getElementById("btn-stop");

async function main() {

    if(localStorage.getItem("Cards") == "0" || localStorage.getItem('Cards') == ""){
        var deck = await createDeck();
        deckId = deck.deck_id;
        localStorage.setItem("deckId", deckId);
    }
    else{
        deckId = localStorage.getItem("deckId"); 
    }
    
    score = document.getElementById("score-flex-container").style.display = "flex";
    displayGameButtons();

    document.addEventListener('keydown', (event) => {
        var key = event.key;
        var stopButton = document.getElementById("btn-stop");
    
        if (key == 'c' && cardValues.length >= 0){
            if (totalScore == 21 || totalScore > 21){
                event.preventDefault();
            }
            else{
                cancel();
            }
        }
        else if (key == 'd'){
            
            if (totalScore == 21 || totalScore > 21){
                event.preventDefault();
            }
            else if (stopButton.hasAttribute('disabled')){
                event.preventDefault();
            }
            else{
                callDrawCard();
            }
            
        }
        else if (key == 's' && totalScore > 0){
            if (totalScore == 21 || totalScore > 21){
                event.preventDefault();
            }
            else if (stopButton.hasAttribute('disabled')){
                event.preventDefault();
            }
            else{
                stopGame();
            }
        }
        else if (key == 'r' && totalScore > 0){
            if (totalScore == 21 || totalScore > 21){
                event.preventDefault();
            }
            else{
                restart();
            }
            
        }
    }, false);

    if(localStorage.getItem("Cards") != "" && localStorage.getItem("Cards") != 0 ){
        var arraySavedCard = backupDrawnCards[0].split(",");

        if(arraySavedCard[0] == "0"){
            arraySavedCard.shift();
        }

        var arrayLength = arraySavedCard.length;
     
        const containerCard = document.getElementById("container-draw-card");
        for( i = 0; i < arrayLength; i++){
             let img = document.createElement("img");
             img.setAttribute("id", "card");
             img.src = arraySavedCard[i];
             containerCard.appendChild(img);
        }
        
    }
    
}

function displayGameButtons(){

    var btnDraw = document.getElementById("draw");

    var btnStop = document.getElementById("btn-stop");

    var btnCancel = document.getElementById("cancel");

    var btnRestart = document.getElementById("restart");
    
    var divGameButtons = document.getElementById("game-buttons");

    if(localStorage.getItem("Cards") == "0" || localStorage.getItem('Cards') == ""){
    
        btnDraw.removeAttribute("style");
        divGameButtons.removeAttribute("style");
        btnStop.setAttribute("style", "display: none;");
        btnCancel.setAttribute("style", "display: none;");
        btnRestart.setAttribute("style", "display: none;");

    }
    else{

        btnDraw.removeAttribute("style");
        divGameButtons.removeAttribute("style");
        btnStop.removeAttribute("style");
        btnCancel.removeAttribute("style");
        btnCancel.removeAttribute("hidden");
        btnRestart.removeAttribute("style");
        
    }

}

async function createDeck() {
    
    return fetch("https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1", {
        method: "GET",
        mode: "cors",
        headers: {
            Accept: "application.json"
        }
    }).then(response => {
        if(response.ok) {
            return response.json();
        } 
            return Promise.reject(new Error("Le paquet de cartes n'a pas pu être créé"));
    }).then((datas) => {
            return datas;
    }).catch(function(error) {
        alert(error.message);
    });
}