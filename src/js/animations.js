document.getElementById('create').addEventListener('click',function(){
    const buttonCreate = document.getElementById('create');
    const buttonRules = document.getElementById('rules-btn');

    buttonCreate.style.transform = 'scale(0.1)';
    buttonRules.style.transform = 'scale(0.1)';

    buttonCreate.style.transitionDuration = '7s';
    buttonRules.style.transitionDuration = '7s';

    buttonCreate.style.opacity = '0.2';
    buttonRules.style.opacity = '0.2';
    
    buttonCreate.style.visibility = 'hidden';
    buttonRules.style.visibility = 'hidden';

    setTimeout(myStartDeck, 1000);
});

document.getElementById('deck-card').style.display = "none";

function myStartDeck() {
    document.getElementById('deck-card').style.display = "block";
}