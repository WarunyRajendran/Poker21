var modal = document.getElementById("myModal");

var modal_victory = document.getElementById('modal-victory');

var modal_defeat = document.getElementById('modal-defeat');

var btn = document.getElementById("rules-btn");

var span = document.getElementsByClassName("close")[0];

var span_result = document.getElementsByClassName("close-modal-result")[0];

var span_defeat = document.getElementsByClassName("close-modal-result")[1];

btn.onclick = function(){
    modal.style.display = "block";
}

span.onclick = function(){
    modal.style.display = "none";
}

span_result.onclick = function(){
    modal_victory.style.display = "none";
    modal_victory.setAttribute('aria-modal', 'false');
    restart();
    document.body.style.backgroundColor = "#095712";
}

span_defeat.onclick = function(){
    restart();
    modal_defeat.style.display = "none";
    modal_defeat.setAttribute('aria-modal', 'false');
    document.body.style.backgroundColor = "#095712";
    
}

window.onclick = function(event){
    if(event.target == modal){
        modal.style.display = "none";
    }
}

function openVictoryModal() {
    document.body.style.backgroundColor = "#14340a";
    var modal_victory = document.getElementById('modal-victory');
    modal_victory.style.display = 'flex';
    modal_victory.removeAttribute('aria-hidden');
    modal_victory.setAttribute('aria-modal', 'true');
}

function openDefeatModal() {
    document.body.style.backgroundColor = "#14340a";
    var modal_defeat = document.getElementById('modal-defeat');
    modal_defeat.style.display = 'block';
    modal_defeat.removeAttribute('aria-hidden');
    modal_defeat.setAttribute('aria-modal', 'true');
}