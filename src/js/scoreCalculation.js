var totalScore = 0;

var backupScore = localStorage.getItem('score');

var score = document.getElementById("score-content");
score.innerHTML = backupScore;

var currentDevice = navigator.userAgent;
var regex = /(\bAndroid\b)/;

var cardValues = [];

if(localStorage.getItem("cardValues") != ""){
    cardValues = [localStorage.getItem("cardValues")];
}

async function setValues(CardObj){

    var object = CardObj[0].value;

    switch (object) {
        case "KING":
        case "QUEEN":
        case "JACK": 
            CardObj[0]["score"] = 10;
            break;
        case "ACE":
            CardObj[0]["score"] = 0;
            break;
        case "2":
            CardObj[0]["score"] = 2;
            break;
        case "3":
            CardObj[0]["score"] = 3;
            break;
        case "4":
            CardObj[0]["score"] = 4;
            break;
        case "5":
            CardObj[0]["score"] = 5;
            break;
        case "6":
            CardObj[0]["score"] = 6;
            break;
        case "7":
            CardObj[0]["score"] = 7;
            break;
        case "8":
            CardObj[0]["score"] = 8;
            break;
        case "9":
            CardObj[0]["score"] = 9;
            break;
        case "10": 
            CardObj[0]["score"] = 10;
            break;

    }

}

async function setValuesNumberCardArbitrary(CardObj, cardArbitrary){

    var object = CardObj[0].value;

    for (let i = 0; i < cardArbitrary; i++) {
        var object = CardObj[i].value;

        switch (object) {
            case "KING":
            case "QUEEN":
            case "JACK": 
                CardObj[i]["score"] = 10;
                break;
            case "ACE":
                CardObj[i]["score"] = 0;
                break;
            case "2":
                CardObj[i]["score"] = 2;
                break;
            case "3":
                CardObj[i]["score"] = 3;
                break;
            case "4":
                CardObj[i]["score"] = 4;
                break;
            case "5":
                CardObj[i]["score"] = 5;
                break;
            case "6":
                CardObj[i]["score"] = 6;
                break;
            case "7":
                CardObj[i]["score"] = 7;
                break;
            case "8":
                CardObj[i]["score"] = 8;
                break;
            case "9":
                CardObj[i]["score"] = 9;
                break;
            case "10": 
                CardObj[i]["score"] = 10;
                break;

        }
    }

} 

async function calculateScore(CardObj){

    var value = CardObj[0].score; 

    backupScore = localStorage.getItem('score');

    totalScore = Number(value) + Number(backupScore);

    localStorage.setItem('score', totalScore);

    var element = document.getElementById("score-content");

    element.innerHTML = totalScore;

    if(totalScore == 21){
        cardAnimationEndGame();
        setTimeout(openVictoryModal, 1000);
        var scoreVictory = document.getElementById('modal-score-victory');
        scoreVictory.textContent = "Score : " + totalScore;
        
        if(currentDevice.match(regex)){
            vibrate();
        }
        var btnDraw = document.getElementById("draw");
        var btnStop = document.getElementById("btn-stop");
        btnDraw.setAttribute("style", "display: none;");
        btnStop.setAttribute("style", "display: none;");
    }
    else if(totalScore > 21){
        cardAnimationEndGame();
        setTimeout(openDefeatModal, 1000);
        var scoreDefeat = document.getElementById("modal-score-defeat");
        scoreDefeat.textContent = "Score: " + totalScore;

        if(currentDevice.match(regex)){
            vibrate();
        }
        var btnDraw = document.getElementById("draw");
        var btnStop = document.getElementById("btn-stop");

        btnDraw.setAttribute("style", "display: none;");
        btnStop.setAttribute("style", "display: none;");
    }

    return totalScore;

}

async function calculateScoreAfterStop(CardObj){
    var value = CardObj[0].score;

    backupScore = localStorage.getItem('score');

    totalScore = Number(value) + Number(backupScore);
    
    var score = totalScore - value;
    
    var element = document.getElementById("score-content");
    element.innerHTML = totalScore;

    if(totalScore > 21){
        cardAnimationEndGame();
        setTimeout(openVictoryModal, 1000);
        var scoreVictory = document.getElementById('modal-score-victory');
        scoreVictory.textContent = "Score : " + score;
        var nextCard = document.getElementById('next-card-value-victory');
        nextCard.textContent = "Valeur de la carte suivante : " + value;

        var scoreVictory = document.getElementById('modal-score-victory');
        scoreVictory.textContent = "Score : " + score;
       
        if(currentDevice.match(regex)){
            vibrate();
        }

        btnCancel = document.getElementById("cancel");
        btnCancel.setAttribute("style", "display: none;");
    }
    else{
        element.innerHTML = score;
        cardAnimationEndGame();
        setTimeout(openDefeatModal, 1000);
        var scoreDefeat = document.getElementById("modal-score-defeat");
        scoreDefeat.textContent = "Score: " + score;
        var nextCardValue = document.getElementById('next-card-value-defeat');
        nextCardValue.textContent = "Valeur de la carte suivante : " + value;

        if(currentDevice.match(regex)){
            vibrate();
        }

        btnCancel = document.getElementById("cancel");
        btnCancel.setAttribute("style", "display: none;");
    }
}

async function calculateScoreAfterCancel(){

    if(cardValues.length)
    {
        var arraySavedValues = cardValues[0].split(",");
        cardValues = arraySavedValues;

        var value = cardValues.pop();
        localStorage.setItem('cardValues', cardValues);

    }

    var savedScore = localStorage.getItem('score');
    
    totalScore = Number(savedScore) - value;
    
    var element = document.getElementById("score-content");
    element.innerHTML = totalScore;

    if(cardValues.length <= 0){
        var btnRestart = document.getElementById("restart");
        var btnCancel = document.getElementById("cancel");
    
        btnRestart.setAttribute("style", "display: none;");
        btnCancel.setAttribute("hidden", "true");
        stopButton.setAttribute("style", "display: none;");
    }
}

async function calculateScoreNumberCardArbitrary(CardObj, cardArbitrary){

    var value = CardObj[0].score; 

    for(let i = 0; i < cardArbitrary; i++)
    {
        value = CardObj[i].score;
        totalScore = value + totalScore;
    }
    
    var element = document.getElementById("score-content");

    element.innerHTML = totalScore;

    if(totalScore == 21){
        setTimeout(openVictoryModal, 1000);
        var scoreVictory = document.getElementById('modal-score-victory');
        scoreVictory.textContent = "Score : " + totalScore;
        
        if(currentDevice.match(regex)){
            vibrate();
        }

        var btnDraw = document.getElementById("draw");
        var btnStop = document.getElementById("btn-stop");
        btnDraw.setAttribute("style", "display: none;");
        btnStop.setAttribute("style", "display: none;");
    }
    else if(totalScore > 21){
        setTimeout(openDefeatModal, 1000);
        var scoreDefeat = document.getElementById("modal-score-defeat");
        scoreDefeat.textContent = "Score: " + totalScore;

        if(currentDevice.match(regex)){
            vibrate();
        }
        
        var btnDraw = document.getElementById("draw");
        var btnStop = document.getElementById("btn-stop");

        btnDraw.setAttribute("style", "display: none;");
        btnStop.setAttribute("style", "display: none;");
    }

    return totalScore;

} 

async function calculateScoreNumberCardArbitraryeAfterStop(CardObj, cardArbitrary){

    var value = 0;

    for(let i = 0; i < cardArbitrary; i++) {
        value = CardObj[i].score;
        totalScore = value + totalScore;
    }
    
    var element = document.getElementById("score-content");
    element.innerHTML = totalScore;

    var score = totalScore - value;

    if(totalScore > 21){
        openVictoryModal();
        var scoreVictory = document.getElementById('modal-score-victory');
        scoreVictory.textContent = "Score : " + score;
        var nextCard = document.getElementById('next-card-value-victory');
        nextCard.textContent = "Valeur de la carte suivante : " + value;

        var scoreVictory = document.getElementById('modal-score-victory');
        scoreVictory.textContent = "Score : " + score;
       
        if(currentDevice.match(regex)){
            vibrate();
        }

        btnCancel = document.getElementById("cancel");
        btnCancel.setAttribute("style", "display: none;");
    }
    else{
        element.innerHTML = score;
        openDefeatModal();
        var scoreDefeat = document.getElementById("modal-score-defeat");
        scoreDefeat.textContent = "Score: " + score;
        var nextCardValue = document.getElementById('next-card-value-defeat');
        nextCardValue.textContent = "Valeur de la carte suivante : " + value;

        if(currentDevice.match(regex)){
            vibrate();
        }

        btnCancel = document.getElementById("cancel");
        btnCancel.setAttribute("style", "display: none;");
    }
} 

function cardAnimationEndGame(){

    var container = document.getElementById("container-draw-card");

    for(i=1; i <= increment; i++){
        var string = i.toString();
        document.getElementById(string).setAttribute("class", "rotate");
    }

    container.removeAttribute("id");
    setTimeout(setClassForCards, 1000);
    increment = 0;
}

function setClassForCards(){
    var container = document.getElementsByClassName('card-container')[0];
    container.setAttribute("id", "container-draw-card");
}