async function stopGame(){

    stopButton = document.getElementById("btn-stop");

    stopButton.setAttribute("disabled", "");

    var start = document.getElementById("create");
    
    var drawCard = document.getElementById("draw");

    start.setAttribute("disabled", "");
    
    drawCard.setAttribute("disabled", "");

    callDrawCardAfterStop();
}