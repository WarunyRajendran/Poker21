const btnStartGame = document.getElementById('create');
const Restart = document.getElementById('restart');
const HiddenSelectAfterRestart = document.getElementById('close-modal');
const CloseModal = document.getElementById('close-modal-defeat');


btnStartGame.addEventListener('click', function() {
    document.getElementById('card-arbitrary').style.display = 'block';
})

function getNumberCardArbitrary() {
    const selectNumber = document.querySelector('select')
    selectNumber.addEventListener('change', function() {
        cardArbitrary = this.value;
    })
    return cardArbitrary;

}

HiddenSelectAfterRestart.addEventListener('click', function() {
    document.querySelector('select').value = 0;
})

CloseModal.addEventListener('click', function() {
    document.querySelector('select').value = 0;
})

Restart.addEventListener('click', function() {
    document.querySelector('select').value = 0;
})