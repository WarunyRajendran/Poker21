function restart(){
    increment = 0;
    cardArbitrary = 0;
    ReturnAllCardsToDeck(deckId);
    const containerDrawCard =  document.getElementById("container-draw-card");
    while (containerDrawCard.firstChild) {
        containerDrawCard.removeChild(containerDrawCard.firstChild);
    }
    document.getElementById("rest-card").innerHTML = "";
    totalScore = 0;
    localStorage.setItem('score', totalScore);
    localStorage.setItem("Cards", backupDrawnCards.length = 0);
    localStorage.setItem('cardValues', cardValues.length = 0);

    var element = document.getElementById("score-content");
    element.innerHTML = totalScore;

    var btnRestart = document.getElementById("restart");
    var btnCancel = document.getElementById("cancel");
    var drawCard = document.getElementById("draw");

    btnRestart.setAttribute("style", "display: none;");
  
    btnCancel.setAttribute("hidden", "true");
    stopButton.setAttribute("style", "display: none;");

    drawCard.removeAttribute("disabled", "");
    drawCard.removeAttribute("style");
    stopButton.removeAttribute("disabled", "");

}


async function ReturnAllCardsToDeck(deck_id) {
    
    return fetch(`https://deckofcardsapi.com/api/deck/${deck_id}/return/`, {
        method: "GET",
        mode: "cors",
        headers: {
            Accept: "application.json"
        }
    }).then(response => {
        if(response.ok) {
            return response.json();
        } 
            return Promise.reject(new Error("Les cartes n'ont pas pu être remises dans le paquet"));
    }).then((datas) => {
            return datas;
    }).catch(function(error) {
        alert(error.message);
    });
}